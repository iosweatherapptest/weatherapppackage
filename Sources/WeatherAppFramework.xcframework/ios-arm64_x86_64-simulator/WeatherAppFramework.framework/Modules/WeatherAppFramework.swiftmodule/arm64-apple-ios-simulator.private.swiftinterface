// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7 (swiftlang-5.7.0.127.4 clang-1400.0.29.50)
// swift-module-flags: -target arm64-apple-ios16.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name WeatherAppFramework
// swift-module-flags-ignorable: -enable-bare-slash-regex
import Foundation
import Swift
@_exported import WeatherAppFramework
import _Concurrency
import _StringProcessing
public struct Alert : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Rain : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Daily : Swift.Codable {
  public let dt: Swift.Int?, sunrise: Swift.Int?, sunset: Swift.Int?, moonrise: Swift.Int?
  public let moonset: Swift.Int?
  public let moonPhase: Swift.Double?
  public let temp: WeatherAppFramework.Temp?
  public let feelsLike: WeatherAppFramework.FeelsLike?
  public let pressure: Swift.Int?, humidity: Swift.Int?
  public let dewPoint: Swift.Double?, windSpeed: Swift.Double?
  public let windDeg: Swift.Int?
  public let weather: [WeatherAppFramework.Weather]?
  public let clouds: Swift.Int?
  public let pop: Swift.Double?, rain: Swift.Double?, uvi: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct FeelsLike : Swift.Codable {
  public let day: Swift.Double?, night: Swift.Double?, eve: Swift.Double?, morn: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Temp : Swift.Codable {
  public let day: Swift.Double?, min: Swift.Double?, max: Swift.Double?, night: Swift.Double?
  public let eve: Swift.Double?, morn: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Minutely : Swift.Codable {
  public let dt: Swift.Int?
  public let precipitation: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Environment {
  public enum APIError : Swift.Error {
    case decodingFailer
    case networkFailer
    case invalidURL
    public static func == (a: WeatherAppFramework.Environment.APIError, b: WeatherAppFramework.Environment.APIError) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum API {
  }
}
public struct Current : Swift.Codable {
  public let dt: Swift.Int?, sunrise: Swift.Int?, sunset: Swift.Int?
  public let temp: Swift.Double?, feelsLike: Swift.Double?
  public let pressure: Swift.Int?, humidity: Swift.Int?
  public let dewPoint: Swift.Double?, uvi: Swift.Double?
  public let clouds: Swift.Int?, visibility: Swift.Int?
  public let windSpeed: Swift.Double?
  public let windDeg: Swift.Int?
  public let weather: [WeatherAppFramework.Weather]?
  public let rain: WeatherAppFramework.Rain?
  public let windGust: Swift.Double?
  public let pop: Swift.Int?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public class WeatherSDK {
  public init(apiKey: Swift.String)
  public func getCurrentWeather(from lat: Swift.String, lon: Swift.String, completion: @escaping (Swift.Result<WeatherAppFramework.MeteoResponse, WeatherAppFramework.Environment.APIError>) -> Swift.Void)
  @objc deinit
}
public struct MeteoResponse : Swift.Codable {
  public let lat: Swift.Double?, lon: Swift.Double?
  public let timezone: Swift.String?
  public let timezoneOffset: Swift.Int?
  public let current: WeatherAppFramework.Current?
  public let minutely: [WeatherAppFramework.Minutely]?
  public let hourly: [WeatherAppFramework.Current]?
  public let daily: [WeatherAppFramework.Daily]?
  public let alerts: [WeatherAppFramework.Alert]?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Weather : Swift.Codable {
  public let id: Swift.Int?
  public let main: Swift.String?, weatherDescription: Swift.String?, icon: Swift.String?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
extension WeatherAppFramework.Environment.APIError : Swift.Equatable {}
extension WeatherAppFramework.Environment.APIError : Swift.Hashable {}

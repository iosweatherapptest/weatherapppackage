import XCTest
@testable import WeatherAppPackage

final class WeatherAppPackageTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(WeatherAppPackage().text, "Hello, World!")
    }
}
